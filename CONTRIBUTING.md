Contributing
============

Please note that this project is released with a Contributor Code of Conduct. By participating in this project you agree to abide by its terms.

See: [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md)


## Help us build a software platform to coordinate Search-And-Rescue operations!

Hi from OneFleet!

We are a team of open-source software developers who aim to improve communication between various actors of rescue operations. We're currently building a live geographic information system ("OneFleet") that helps different organisations track their ships and better coordinate their missions. It enables them to communicate time-critical geographical information more efficiently, thereby increasing the chances of rescue for people in distress. 

We are endorsed by multiple Search-and-Rescue organisations and OneFleet already won some basic financial backing as part of Round 7 of the Open Knowledge Foundation's Prototype Fund in 2020.


### Who we are looking for
OneFleet is in active development and part of it is already being tested. But there is still a lot to do! Therefore, we are looking for new members in our team. We are looking for people who have some free time (or more) to support our open-source project and thereby help improve the work of NGOs in the field of Search-And-Rescue.

### How we work and how you can help
We meet up online regularly (every 2-4 weeks) to discuss the current state, issues and developments. In between, we often meet spontaneously, code together, and sometimes have a beer and a little chat along that. 

If you were to join our team, it would be important that you can join our meetings. Your main tasks would be working on open issues, fixing bugs and implementing new features. 

### Skills we crave
Any of the following skills will be very useful, but if you have other things to offer, don't hesitate to contact us!
* UX design / UX testing
* TypeScript / VueJS
* Security engineers

### Our Stack

• VueJS with Element UI framework 
• node.js + TypeScript
• Leaflet framework to display the maps.
• Microservices for data intake
• Backend: Couch DB



### What to expect :)
We are a small team with different backgrounds. Some of us have lots of experiences, others have just started out. Some of us can invest a lot of time, some can't. But we all take the project very seriously and are excited to see it in use!

We have a flexible schedule and there is always room to bring up your own ideas, and to implement them. 

### How to reach us 
If you are interested or have questions, contact us here at gitlab. We have also prepared screencasts to introduce you to the source code we can then share with you!

