import { caseFilters } from './caseFilters';
import { civilfleetFilters } from './civilfleetFilters';
import { landmarkFilters } from './landmarkFilters';
import { commercialFilters } from './commercialFilters';
import { militaryFilters } from './militaryFilters';
import { sightingFilters } from './sightingFilters';
import { otherFilters } from './otherFilters';
import { ItemFilterGroup } from '@/types/item-filter';

/**
 * The Filters singleton class.
 * Defines which types of items should be shown in which
 * navigation bar tab and/or map layer in the UI
 */
export class Filters {
  /**
   * The available filters.
   */
  public static readonly filters = [
    caseFilters,
    civilfleetFilters,
    landmarkFilters,
    commercialFilters,
    militaryFilters,
    sightingFilters,
    otherFilters,
  ];

  /**
   * Get all filters.
   * @returns All filters.
   */
  public getAll(): typeof Filters.filters {
    return Filters.filters;
  }

  /**
   * Get all filters as an array.
   * @returns All filters.
   */
  public get get_filter_groups(): ItemFilterGroup[] {
    return Filters.filters;
  }
}

export default new Filters();
