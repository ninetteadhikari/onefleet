import { ItemFilterGroup } from '@/types/item-filter';

export const sarFilters: ItemFilterGroup = {
  title: 'SAR Zones',
  selectable_in_sidebar: false,
  selectable_on_map: false, // TODO: implement item zones
  initially_selected_on_map: true,
  filters: [
    {
      name: 'Filtergroup Templates',
      field: 'template',
      values: ['sarzone'],
      always_active: true,
    },
    {
      name: 'Only Central Med',
      field: 'properties.category',
      values: ['centralmed'],
    },
  ],
  sortings: [],
};
