import filters from './index';

describe('filters', () => {
  const all_filters = filters.get_filter_groups;

  describe('.get_filter_groups()', () => {
    it('should return a list of filters', () => {
      expect(
        all_filters.length >= 1 &&
          all_filters.every(filter_group => typeof filter_group === 'object')
      ).toBe(true);
    });
  });
});
