import { ItemFilterGroup } from '@/types/item-filter';

export const landmarkFilters: ItemFilterGroup = {
  title: 'Landmarks',
  selectable_in_sidebar: true,
  selectable_on_map: true,
  initially_selected_on_map: true,
  filters: [
    {
      name: 'Filtergroup Templates',
      field: 'template',
      values: ['landmark'],
      always_active: true,
    },
    {
      name: 'Hide Oil Fields',
      field: 'properties.category',
      values: ['!oil_field'],
    },
    {
      name: 'Hide Sand Banks',
      field: 'properties.category',
      values: ['!sand_bank'],
    },
    {
      name: 'Hide Islands',
      field: 'properties.category',
      values: ['!island'],
      initially_active: true,
    },
    {
      name: 'Hide Beaches',
      field: 'properties.category',
      values: ['!beach'],
    },
    {
      name: 'Hide Cities',
      field: 'properties.category',
      values: ['!city'],
    },
  ],
  sortings: [],
};
