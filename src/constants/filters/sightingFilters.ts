import { ItemFilterGroup } from '@/types/item-filter';

export const sightingFilters: ItemFilterGroup = {
  title: 'Sightings',
  selectable_in_sidebar: true,
  selectable_on_map: true,
  initially_selected_on_map: true,
  filters: [
    {
      name: 'Filtergroup Templates',
      field: 'template',
      values: ['sighting'],
      always_active: true,
    },
    {
      name: 'Category: Coastguard',
      field: 'properties.category',
      values: ['coastguard'],
    },
    {
      name: 'Flagstate: EU',
      field: 'properties.flagstate',
      values: ['EU'],
    },
    {
      name: 'Flagstate: IT',
      field: 'properties.flagstate',
      values: ['IT'],
    },
    {
      name: 'Flagstate: LYB',
      field: 'properties.flagstate',
      values: ['LYB'],
    },
    {
      name: 'Flagstate: MALTA',
      field: 'properties.flagstate',
      values: ['MALTA'],
    },
  ],
  sortings: [],
};
