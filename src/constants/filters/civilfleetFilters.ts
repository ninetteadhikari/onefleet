import { ItemFilterGroup } from '@/types/item-filter';

export const civilfleetFilters: ItemFilterGroup = {
  title: 'Civil Fleet',
  selectable_in_sidebar: true,
  selectable_on_map: true,
  initially_selected_in_sidebar: true,
  initially_selected_on_map: true,
  filters: [
    {
      name: 'Filtergroup Templates',
      field: 'template',
      values: ['vehicle', 'aircraft'],
      always_active: true,
    },
    {
      name: 'Filtergroup Categories',
      field: 'properties.category',
      values: ['civilfleet'],
      always_active: true,
    },
    {
      name: 'Active Vehicles',
      field: 'properties.active',
      values: ['true'],
      initially_active: true,
    },
    {
      name: 'Only Aircraft',
      field: 'properties.air',
      values: ['true'],
    },
    {
      name: 'Only Ships',
      field: 'properties.air',
      values: ['false'],
    },
    // Not yet implemented:
    // {
    //   name: 'Last Position < 5 days ago',
    //   field: 'position.timestamp',
    //   values: ['$gte($now - 5)'],
    // },
  ],
  sortings: [],
};
