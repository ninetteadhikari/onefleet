import { ItemFilterGroup } from '@/types/item-filter';

export const caseFilters: ItemFilterGroup = {
  title: 'Cases',
  selectable_in_sidebar: true,
  selectable_on_map: true,
  initially_selected_on_map: true,
  filters: [
    {
      name: 'Filtergroup Template',
      field: 'template',
      values: ['case'],
      always_active: true,
    },
    {
      name: 'Status: Open',
      field: 'properties.status',
      values: [
        'open_confirmed',
        'open_lost',
        'open_unknown',
        'approaching',
        'disembarked', // future corrected template soon?
        'diembarked',
        'attending',
        'open_attended', // deprecated, but some cases might still use this status
        'attended', // deprecated, but some cases might still use this status
        'unattended', // deprecated, but some cases might still use this status
        'critical', // deprecated, but some cases might still use this status
        'confirmed', // deprecated, but some cases might still use this status
        'lost', // deprecated, but some cases might still use this status
        'unknown', // deprecated, but some cases might still use this status
        'possible_target', // deprecated, but some cases might still use this status
      ],
      initially_active: true,
    },
    {
      name: 'Status: Closed',
      field: 'properties.status',
      values: [
        'closed_rescued', // deprecated, but some cases might still use this status
        'closed_intercepted', // deprecated, but some cases might still use this status
        'closed_shipwrecked', // deprecated, but some cases might still use this status
        'closed_arrived', // deprecated, but some cases might still use this status
        'closed',
      ],
    },
    {
      name: 'Engine Working: No',
      field: 'properties.engine_working',
      values: [
        'false', // deprecated, but some cases might still use this status
        'no',
      ],
    },
    // Not yet implemented:
    // {
    //   name: 'First Seen in last 7 days',
    //   field: 'properties.first_seen',
    //   values: ['$gte(now - 7)'],
    // },
    // {
    //   name: 'Last Position < 2 days ago',
    //   field: 'position.timestamp',
    //   values: ['$gte(now - 2)'],
    // },
  ],
  sortings: [],
};
