import { ItemFilterGroup } from '@/types/item-filter';

export const militaryFilters: ItemFilterGroup = {
  title: 'Military',
  selectable_in_sidebar: true,
  selectable_on_map: true,
  initially_selected_on_map: true,
  filters: [
    {
      name: 'Filtergroup Templates',
      field: 'template',
      values: ['vehicle', 'aircraft'],
      always_active: true,
    },
    {
      name: 'Filtergroup Categories',
      field: 'properties.category',
      values: ['military'],
      always_active: true,
    },
    {
      name: 'Active Vehicles',
      field: 'properties.active',
      values: ['true'],
      initially_active: true,
    },
    {
      name: 'Only Aircraft',
      field: 'properties.air',
      values: ['true'],
    },
    {
      name: 'Only Ships',
      field: 'properties.air',
      values: ['false'],
    },
  ],
  sortings: [],
};
