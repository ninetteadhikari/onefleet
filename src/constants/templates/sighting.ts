import { ItemTemplate } from '@/types/item-template-schema';

export const sighting: ItemTemplate = {
  plural: 'Sightings',
  pouch_identifier: 'SIGHTING',
  add_initial_position: true,
  type: 'line',
  fields: [
    {
      name: 'name',
      title: 'Name',
      type: 'text',
    },
    {
      name: 'category',
      title: 'Sighting Category',
      type: 'select',
      options: {
        military: 'Military Spotting',
        coastguard: 'Coast Guard Spotting',
        unknown: 'Unknown',
      },
    },
    {
      name: 'flagstate',
      title: 'Possible Flagstate',
      type: 'text',
    },
    {
      name: 'asset',
      title: 'Asset',
      type: 'text',
    },
    {
      name: 'time',
      title: 'Time',
      type: 'text',
    },
    {
      name: 'comment',
      title: 'Comments',
      type: 'text',
    },
  ],
};
