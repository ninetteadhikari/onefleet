import { ItemTemplate } from '@/types/item-template-schema';

export const vehicle: ItemTemplate = {
  plural: 'Vehicles',
  pouch_identifier: 'VEHICLE',
  add_initial_position: false,
  type: 'line',
  fields: [
    {
      name: 'name',
      title: 'Ship Name',
      type: 'text',
    },
    {
      name: 'category',
      title: 'Affiliation Category',
      type: 'select',
      options: {
        unknown: 'Still Unknown',
        import: 'Import',
        civilfleet: 'Civilfleet',
        commercial: 'Commercial',
        military: 'Military',
        other: 'Other',
      },
    },
    {
      name: 'air',
      title: 'Air',
      type: 'select',
      options: {
        false: 'false',
        // true: 'true',
      },
      value: 'false',
      hidden: true,
    },
    {
      name: 'tracking_type',
      title: 'Tracking Type',
      type: 'select',
      options: {
        MANUAL: 'No Automatic Tracking',
        AIS: 'AIS',
        IRIDIUMGO: 'IRIDIUMGO',
      },
      value: 'MANUAL',
    },
    {
      name: 'get_historical_data_since',
      title: 'Get historical data since (days)',
      type: 'number',
      value: -1,
      step: 1,
    },
    {
      name: 'MMSI',
      title: 'MMSI',
      type: 'number',
      step: 1,
    },
    {
      name: 'iridium_sender_mail',
      title: 'Iridium Sender Mail (for iridium tracking only)',
      type: 'text',
    },
    {
      name: 'color',
      title: 'Color',
      type: 'color',
    },
    {
      name: 'fleetmon_vessel_id',
      title: 'Fleetmon Vessel ID',
      type: 'number',
      hidden: true,
    },
    {
      name: 'flagstate',
      title: 'Flagstate',
      type: 'text',
    },
    {
      name: 'active',
      title: 'Active',
      type: 'select',
      options: {
        true: 'true',
        false: 'false',
      },
      value: 'true',
    },
  ],
};
