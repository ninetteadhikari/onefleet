import { ItemTemplate } from '@/types/item-template-schema';

export const landmark: ItemTemplate = {
  plural: 'Landmarks',
  pouch_identifier: 'LANDMARK',
  add_initial_position: true,
  type: 'line',
  fields: [
    {
      name: 'name',
      title: 'Name',
      type: 'text',
    },
    {
      name: 'category',
      title: 'Landmark Category',
      type: 'select',
      options: {
        oil_field: 'Oil Field / Wind Farm',
        sand_bank: 'Sand Bank',
        island: 'Island Centre',
        beach: 'Beach',
        city: 'City/Port',
      },
    },
    {
      name: 'comment',
      title: 'Comments',
      type: 'text',
    },
  ],
};
