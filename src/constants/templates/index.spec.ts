import templates from './index';

describe('templates', () => {
  const templateKeys = templates.keys;

  describe('.keys()', () => {
    it('should return a list of template keys', () => {
      expect(
        templateKeys.length >= 1 &&
          templateKeys.every(key => typeof key === 'string')
      ).toBe(true);
    });
  });

  describe('.getAll()', () => {
    it(`should return all templates`, () => {
      const allTemplates = templates.getAll();
      expect(templateKeys.every(key => !!allTemplates[key])).toBe(true);
    });
  });

  describe('.get()', () => {
    it('should return a single template for every key', () => {
      expect(
        templateKeys.every(key => typeof templates.get(key) === 'object')
      ).toBe(true);
    });
  });
});
