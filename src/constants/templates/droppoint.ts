import { ItemTemplate } from '@/types/item-template-schema';

export const droppoint: ItemTemplate = {
  plural: 'Droppoint',
  pouch_identifier: 'DROPPOINT',
  add_initial_position: true,
  type: 'point',
  fields: [
    {
      name: 'name',
      title: 'title',
      type: 'text',
    },
    {
      name: 'created_by_vehicle',
      title: 'Created by vehicle',
      type: 'text',
    },
    {
      name: 'comment',
      title: 'comment',
      type: 'text',
    },
  ],
};
