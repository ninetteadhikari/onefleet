import { ItemTemplate } from '@/types/item-template-schema';
import { aircraft } from './aircraft';
import { caseTemplate } from './case';
import { droppoint } from './droppoint';
import { landmark } from './landmark';
import { sighting } from './sighting';
import { vehicle } from './vehicle';

/**
 * The Templates singleton class.
 */
export class Templates {
  /**
   * The available templates.
   */
  public static readonly templates = {
    vehicle,
    aircraft,
    landmark,
    sighting,
    droppoint,
    case: caseTemplate,
  };

  /**
   * Get all available template keys.
   * @returns The available template keys.
   */
  public get keys(): (keyof typeof Templates.templates)[] {
    return Object.keys(
      Templates.templates
    ) as (keyof typeof Templates.templates)[];
  }

  /**
   * Get all templates.
   * @returns All templates.
   */
  public getAll(): typeof Templates.templates {
    return Templates.templates;
  }

  /**
   * Get a single template by it's key.
   * @param name - The template key.
   * @returns A single template.
   */
  public get(name: keyof typeof Templates.templates): ItemTemplate {
    return Templates.templates[name];
  }
}

export default new Templates();
