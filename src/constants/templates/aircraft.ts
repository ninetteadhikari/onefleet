import { ItemTemplate } from '@/types/item-template-schema';

export const aircraft: ItemTemplate = {
  plural: 'Aircraft',
  pouch_identifier: 'AIR',
  add_initial_position: false,
  type: 'line',
  fields: [
    {
      name: 'name',
      title: 'title',
      type: 'text',
    },
    {
      name: 'category',
      title: 'Category',
      type: 'select',
      options: {
        civilfleet: 'Civilfleet',
        import: 'Import',
        commercial: 'Commercial',
        military: 'Military',
        unknown: 'Still Unknown',
        other: 'Other',
      },
    },
    {
      name: 'air',
      title: 'Air',
      type: 'select',
      options: {
        // false: 'false',
        true: 'true',
      },
      value: 'true',
      hidden: true,
    },
    {
      name: 'tracking_type',
      title: 'Tracking Type',
      type: 'select',
      options: {
        MANUAL: 'No Automatic Tracking',
        // AIS: 'AIS',
        IRIDIUMGO: 'IRIDIUMGO',
        ADSB: 'ADS-B',
      },
      value: 'MANUAL',
    },
    {
      name: 'get_historical_data_since',
      title: 'Get historical data since (days)',
      type: 'number',
      value: -1,
      step: 1,
    },
    {
      name: 'ADSB',
      title: 'ADS-B ID',
      type: 'number',
      step: 1,
    },
    {
      name: 'iridium_sender_mail',
      title: 'Iridium Sender Mail (for iridium tracking only)',
      type: 'text',
    },
    {
      name: 'color',
      title: 'Color',
      type: 'color',
    },
    {
      name: 'fleetmon_vessel_id',
      title: 'Fleetmon Vessel ID',
      type: 'number',
      hidden: true,
    },
    {
      name: 'active',
      title: 'Active',
      type: 'select',
      options: {
        true: 'true',
        false: 'false',
      },
      value: 'true',
    },
  ],
};
