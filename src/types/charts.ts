export interface Dataset {
  label: string;
  backgroundColor: string;
  data: DiagramPoint[];
}

export interface DiagramPoint {
  x: string;
  t: Date;
  y: number;
}
