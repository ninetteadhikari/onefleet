import { Templates } from '@/constants/templates';

export interface DbItem {
  _id: string;
  _rev: string;
  identifier: string;
  template: keyof typeof Templates.templates;
  properties: Record<string, string | undefined>;
  soft_deleted?: boolean; // soft-delete of items should only be allowed if a duplicate exists
}
