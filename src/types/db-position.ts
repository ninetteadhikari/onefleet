export interface DbPositionPartial {
  _id: string; // item_identifier + '_' + timestamp (isotime)
  lat: number;
  lon: number;
  item_identifier: string; // must never change. Rather copy & soft-delete a position.
  source: string;
  timestamp: string; // must never change. Rather copy & soft-delete a position.
}

export interface DbPosition extends DbPositionPartial {
  _rev: string;
  altitude: number;
  heading: number;
  speed: number;
  soft_deleted?: boolean; // invalid positions should never be deleted by the frontend app, only maked as "soft_deleted"
}

export type DbPositionsPerItem = Record<string, Array<DbPosition>>;
