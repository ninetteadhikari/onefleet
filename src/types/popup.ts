import { PositionDoc } from './map-item';

export interface PopupData {
  positions?: PositionDoc[];
  position?: PositionDoc | null;
  distance_in_meters?: number;
  distance_readable?: string;
  item_id?: string;
  item_title?: string;
  area_readable?: string;
  area_positions?: PositionDoc[];
  latest_position?: PositionDoc | null;
}
