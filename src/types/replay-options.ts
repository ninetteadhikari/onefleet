import mapWrapper from '../utils/mapWrapper';

export interface ReplayData {
  startDate: string;
  endDate: string;
  frameLength: number;
  hoursPerFrame: number;
}

export interface ReplayOptions extends ReplayData {
  map: typeof mapWrapper;
  // Todo: Infer this type from vue
  shown_items: Record<string, true>;
}
