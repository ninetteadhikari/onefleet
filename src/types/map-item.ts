export interface PositionDoc {
  timestamp: string;
  heading?: number;
  speed?: number;
  lat: number;
  lon: number;
}

export interface PositionDocEnriched {
  timestamp: Date;
  heading: number;
  lat: number;
  lon: number;
}

export interface MapItem {
  id: string;
  doc: {
    template: string;
    identifier: string;
    properties: {
      icon: string;
      color: string;
      name: string;
    };
  };
  positions: {
    doc: PositionDoc;
  }[];
}
