<template>
  <div id="app">
    <Loadingscreen v-if="show_loadingscreen"></Loadingscreen>
    <Login v-if="modal == 'login'"></Login>
    <Settings v-if="modal == 'settings'"></Settings>

    <CreateItem
      v-if="modal == 'createItem'"
      :given_template="modal_data.given_template_type"
      :given_positions="modal_data.given_positions"
    ></CreateItem>
    <ShowItem
      v-show="modal == 'showItem'"
      :itemId="modal_data.given_item_id"
      :given_positions="modal_data.given_positions"
      :mapped_base_items="mapped_base_items"
      :positions_per_item="positions_per_item"
    ></ShowItem>
    <ExportItem
      v-show="exportItemId != false"
      :exportItemId="exportItemId"
      @set-export-item-id="exportItemId = $event"
    ></ExportItem>

    <TopNavigation
      :main_view="main_view"
      :showing_air="modal == 'showAir'"
      :showing_log="modal == 'showLog'"
      :position_limits="position_limits"
    ></TopNavigation>
    <Air v-if="modal == 'showAir'"></Air>
    <Log v-if="modal == 'showLog'"></Log>

    <LeftNavigation
      v-show="main_view == 'map'"
      :filters="filters"
      :base_items="base_items"
      :filtered_base_items="allFilteredItems"
      :positions_per_item="positions_per_item"
    />
    <div id="mainWindow">
      <MapArea
        v-show="main_view == 'map'"
        :filtered_base_items="allFilteredItems"
        :positions_per_item="positions_per_item"
      />
    </div>
    <ListView
      class="wide"
      v-show="main_view == 'list'"
      :base_items="base_items"
      :positions_per_item="positions_per_item"
    />
  </div>
</template>

<script lang="ts">
import Vue from 'vue';

// vue-components: main window areas
import TopNavigation from './components/TopNavigation.vue';
import LeftNavigation from './components/LeftNavigation.vue';
import MapArea from './components/MapArea.vue';
import ListView from './components/ListView.vue';

// vue-components: item-specific modals
import CreateItem from './components/items/CreateItem.vue';
import ShowItem from './components/items/ShowItem.vue';
import ExportItem from './components/items/ExportItem.vue';

// vue-components: other modals & popups
import Air from './components/Air.vue';
import Log from './components/Log.vue';
import Login from './components/Login.vue';
import Settings from './components/Settings.vue';
import Loadingscreen from './components/Loadingscreen.vue';

// other imports
import all_filters from './constants/filters';
import { eventBus } from '@/eventBus';
import storage from './utils/storageWrapper';
import {
  ItemFilter,
  ItemFilterGroup,
  FilteredItemSection,
} from '@/types/item-filter';
import { DbItem } from '@/types/db-item';
import { DbPosition, DbPositionsPerItem } from '@/types/db-position';

export default Vue.extend({
  name: 'app',
  components: {
    // main window areas:
    TopNavigation,
    LeftNavigation,
    MapArea,
    ListView,
    // item-specific modals:
    CreateItem,
    ShowItem,
    ExportItem,
    // other modals & popups:
    Air,
    Log,
    Login,
    Settings,
    Loadingscreen,
  },
  data: () => ({
    main_view: 'map',
    modal: '',
    modal_data: {},
    show_loadingscreen: true,
    exportItemId: false as string | false,
    base_items: [] as DbItem[],
    positions_per_item: {} as DbPositionsPerItem,
    filters: [] as ItemFilter[][],
    initial_replication_done: false,
    position_limits: {
      tracks_oldest_date_iso: new Date('2019-01-01').toISOString(),
      tracks_newest_date_iso: null as string | null, // now
      tracks_length_limit: 100 as number, // may be overwritten by local storage below
    },
  }),
  computed: {
    /**
     * This computed value just returns an indexed version of the base_items.
     * The index is the item's id field.
     */
    mapped_base_items(): Record<string, DbItem> {
      return this.base_items.reduce((map, item) => {
        map[item._id] = item;
        return map;
      }, {} as Record<string, DbItem>);
    },

    /**
     * This computed value always contains an array of filtered item sections.
     * It is used by the LeftNavigationBar to display all items by tab section,
     * and by the MapArea to display the visible items in one layer per filter section.
     * Some map Popup components also use this to display available items in popups.
     */
    allFilteredItems(): Array<FilteredItemSection> {
      let all_filter_groups: Array<ItemFilterGroup> =
        all_filters.get_filter_groups;
      let filtered_item_groups: Array<FilteredItemSection> = [];

      // set up tabs for the tabs bar so that they are shown even before items are loaded
      for (let s_id in all_filter_groups) {
        // two-stage filtering for computing hidden items per section:
        let active_filters: Array<ItemFilter> = this.filters[s_id].filter(
          f => f.active
        );
        let section_filters: Array<ItemFilter> = active_filters.filter(
          f => f.always_active
        );

        // filter all items for this section:
        let section_base_items: Array<DbItem> = this.base_items.filter(
          base_item =>
            section_filters.every(section_filter =>
              this.matchesFilter(base_item, section_filter)
            )
        );

        // filter additional items based on active filters:
        let filtered_base_items: Array<DbItem> = section_base_items.filter(
          base_item =>
            active_filters.every(active_filter =>
              this.matchesFilter(base_item, active_filter)
            )
        );

        filtered_item_groups.push({
          title: all_filter_groups[s_id].title,
          base_items: filtered_base_items,
          hidden_items: section_base_items.length - filtered_base_items.length,
          filter_group_settings: all_filter_groups[s_id],
        });
      }
      return filtered_item_groups;
    },
  },
  watch: {
    initial_replication_done(): void {
      this.loadItems();
    },
    base_items(): void {
      // only load positions after initial replication has really finished, or we'll not see all requested positions
      if (this.initial_replication_done) this.loadPositionsForItems();
    },
  },
  methods: {
    loadItems(): void {
      this.$db.getBaseItems().then(result => {
        this.base_items = result.rows.map(item => item.doc);
      });
    },
    loadPositionsForItems(): void {
      let positions_per_item: DbPositionsPerItem = {};

      let promises: Array<any> = this.base_items.map(base_item =>
        base_item.identifier
          ? this.$db
              .getPositionsForItemPromise(
                base_item.identifier,
                this.position_limits.tracks_length_limit,
                this.position_limits.tracks_newest_date_iso || '',
                this.position_limits.tracks_oldest_date_iso
              )
              .then((db_positions: Array<DbPosition>) => {
                positions_per_item[base_item.identifier] = db_positions;
                return db_positions[0] ? db_positions[0].timestamp : null;
              })
          : null
      );

      Promise.all(promises).then(latest_position_timestamps => {
        // tell TopNavigation the newest item position:
        eventBus.emit('updateLastPositionTimestamp', {
          lastPositionTimestamp:
            latest_position_timestamps
              .sort()
              .reverse()
              .filter(timestamp => timestamp != null)[0] || null,
        });

        // update all item positions for Vue components to use:
        this.positions_per_item = positions_per_item;
      });
    },
    initTrackSettings(): void {
      this.position_limits.tracks_oldest_date_iso =
        storage.get('settings_track_startdate') ||
        this.position_limits.tracks_oldest_date_iso;
      this.position_limits.tracks_newest_date_iso =
        storage.get('settings_track_enddate') ||
        this.position_limits.tracks_newest_date_iso;

      const storageTrackLength = storage.get('settings_map_track_length');
      this.position_limits.tracks_length_limit = storageTrackLength
        ? parseInt(storageTrackLength, 10)
        : this.position_limits.tracks_length_limit;
    },

    /** Start of Item Filter functions */
    initFilters(): void {
      /** Get filters and set up their active state */
      let all_filter_groups: ItemFilterGroup[] = all_filters.get_filter_groups;
      let filters: ItemFilter[][] = [];
      for (let section_index in all_filter_groups) {
        filters[section_index] = all_filter_groups[section_index].filters.map(
          (filter: ItemFilter) => {
            filter.active =
              filter.always_active || filter.initially_active ? true : false;
            return filter;
          }
        );
      }
      this.filters = filters;
    },
    matchesFilter(base_item: DbItem, filter: ItemFilter): boolean {
      let item_value = filter.field
        .split('.')
        .reduce((o, i) => o[i] || {}, base_item);
      return filter.values.some(filter_value =>
        this.fitsFilterValue(
          typeof item_value === 'string' ? item_value : '',
          filter_value
        )
      );
    },
    fitsFilterValue(item_value: string, filter_value: string): boolean {
      switch (filter_value[0]) {
        case '$':
          return this.evaluateComparisonFunction();
        case '!':
          return item_value != filter_value.substring(1);
        default:
          return item_value == filter_value;
      }
    },
    evaluateComparisonFunction(): true {
      // TODO: use simple regex to implement comparison function. Or Mango-style Query?
      console.log('evaluateComparisonFunction not implemented yet!');
      return true;
    },
    /** End of Item Filter functions */
  },

  created(): void {
    this.initTrackSettings();

    eventBus.on('showLoginScreen', () => {
      this.show_loadingscreen = false;
      this.modal_data = {};
      this.modal = 'login';
    });

    eventBus.on('updateMainView', ({ mainView }) => {
      this.main_view = mainView;
    });
    eventBus.on('showSettings', () => {
      console.log('ettings');
      this.modal = 'settings';
    });
    eventBus.on('createItem', ({ templateKey, positions }) => {
      this.modal_data = {
        given_template_type: templateKey,
        given_positions: positions,
      };
      this.modal = 'createItem';
    });
    eventBus.on('showAir', () => {
      this.modal = 'showAir';
    });
    eventBus.on('showLog', () => {
      this.modal = 'showLog';
    });
    eventBus.on('showSettings', () => {
      this.modal_data = {};
      this.modal = 'settings';
    });
    eventBus.on('showItem', ({ itemId, positions }) => {
      if (itemId) {
        this.modal_data = {
          given_item_id: itemId,
          given_positions: positions,
        };
        // this.itemId = itemId;
        this.modal = 'showItem';
      } else {
        // this.itemId = false;
        this.modal = '';
        this.modal_data = {};
      }
    });
    eventBus.on('updateExportItemId', ({ itemId }) => {
      this.exportItemId = itemId;
    });
    eventBus.on('closeModal', () => {
      this.modal = '';
      this.modal_data = {};
    });

    //set on change listener on positions because its usually the largest database
    this.$db.setOnInitialReplicationDone(
      'positions',
      'hide_loadingscreen',
      () => {
        this.show_loadingscreen = false;
        this.initial_replication_done = true;
      }
    );
    this.$db.setOnChange('items', 'base_items_change', () => {
      //reload base_items if change is detected
      this.loadItems();
    });
    this.$db.setOnChange('positions', 'base_positions_change', () => {
      //reload positions if change is detected
      this.loadPositionsForItems();
      eventBus.emit('updateLastChangeDate', { date: new Date() });
    });

    this.initFilters();
  },
});
</script>

<style>
:root {
  --primary-darker: #04202e;
  --primary-dark: #1079ad;
  --primary: #17affa;
  --primary-light: #61c7fa;
  --primary-lighter: #def1fa;
  --white: #ffffff;
  --light-gray: #eeeeee;
  --dark-gray: #666666;
  --black: #151515;
  --red: #ff3333;

  --font-family-sans-serif: -apple-system, BlinkMacSystemFont, 'Segoe UI',
    Roboto, 'Noto Sans', Ubuntu, Cantarell, 'Helvetica Neue', sans-serif,
    'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol', 'Noto Color Emoji';
  --font-family-monospace: 'Menlo', 'DejaVu Sans Mono', 'Liberation Mono',
    'Consolas', 'Ubuntu Mono', 'Courier New', 'andale mono', 'lucida console',
    monospace;
  --border-radius: 4px;
  --app-top: 40px;
  --app-left-siderbar: 280px;
}

.el-button--danger {
  background: var(--red);
}

.el-tabs__item.is-active {
  color: var(--primary);
}

.svg-inline--fa {
  margin-right: 0.25em;
}

body {
  margin: 0;
  font-family: var(--font-family-sans-serif);
  font-size: 1rem;
  font-weight: 400;
  line-height: 1.5;
  color: var(--black);
  text-align: left;
  background-color: var(--white);
}

#app {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  padding-top: var(--app-top);
}

.hide_chat {
  margin-right: -20vw;
}

#mainWindow {
  position: absolute;
  width: -webkit-calc(100% - var(--app-left-siderbar));
  width: -moz-calc(100% - var(--app-left-siderbar));
  width: calc(100% - var(--app-left-siderbar));
  left: var(--app-left-siderbar);
  right: 0px;
  top: var(--app-top);
  bottom: 0px;
  background: var(--white);
}

.wide {
  position: absolute;
  left: 10px;
  right: 10px;
  top: var(--app-top);
  bottom: 0px;
  background: var(--white);
}

#chat {
  display: none; /* hide for now */
  position: fixed;
  width: 20vw;
  right: 0;
  top: var(--app-top);
  bottom: 0;
  z-index: 999;
}

ul {
  list-style: none;
  margin: 0;
  padding: 0;
}

.background {
  position: fixed;
  overflow: auto;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: rgba(0, 0, 0, 0.8);
  z-index: 9999;
}

.form-style-6 {
  max-width: 400px;
  margin: 10px auto;
  padding: 16px;
  background: var(--white);
}
.form-style-6 h1 {
  background: var(--primary);
  padding: 20px 0;
  font-size: 140%;
  font-weight: 300;
  text-align: center;
  color: var(--white);
  margin: -16px -16px 16px -16px;
}
.form-style-6 input[type='text'],
.form-style-6 input[type='date'],
.form-style-6 input[type='datetime'],
.form-style-6 input[type='email'],
.form-style-6 input[type='number'],
.form-style-6 input[type='search'],
.form-style-6 input[type='time'],
.form-style-6 input[type='url'],
.form-style-6 input[type='password'],
.form-style-6 input[type='checkbox'],
.form-style-6 input[type='datetime-local'],
.form-style-6 textarea,
.form-style-6 select {
  transition: all 0.3s ease-in-out;
  -webkit-transition: all 0.3s ease-in-out;
  -moz-transition: all 0.3s ease-in-out;
  -ms-transition: all 0.3s ease-in-out;
  -o-transition: all 0.3s ease-in-out;
  outline: none;
  box-sizing: border-box;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  width: 100%;
  background: var(--white);
  margin-bottom: 4%;
  border: 1px solid var(--light-gray);
  padding: 3%;
  color: var(--dark-gray);
  font: 95% Arial, Helvetica, sans-serif;
  height: 42px;
}

.form-style-6 input[type='checkbox'] {
  height: 10px;
  width: auto;
}

.form-style-6 input[type='text']:focus,
.form-style-6 input[type='date']:focus,
.form-style-6 input[type='datetime']:focus,
.form-style-6 input[type='email']:focus,
.form-style-6 input[type='number']:focus,
.form-style-6 input[type='search']:focus,
.form-style-6 input[type='time']:focus,
.form-style-6 input[type='url']:focus,
.form-style-6 input[type='password']:focus,
.form-style-6 textarea:focus,
.form-style-6 select:focus {
  box-shadow: 0 0 5px var(--primary);
  padding: 3%;
  border: 1px solid var(--primary);
}

.form-style-6 input[type='submit'],
.form-style-6 button,
.form-style-6 input[type='button'] {
  box-sizing: border-box;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  width: 100%;
  padding: 3%;
  background: var(--primary);
  border-bottom: 2px solid var(--primary);
  border-top-style: none;
  border-right-style: none;
  border-left-style: none;
  color: #fff;
  cursor: pointer;
}
.form-style-6 input[type='submit']:hover,
.form-style-6 input[type='button']:hover {
  background: var(--primary-dark);
}
.select-css {
  display: block;
  font-size: 16px;
  font-family: sans-serif;
  font-weight: 700;
  color: var(--dark-gray);
  line-height: 1.3;
  padding: 0.6em 1.4em 0.5em 0.8em;
  width: 100%;
  max-width: 100%;
  box-sizing: border-box;
  margin: 0;
  border: 1px solid var(--black);
  border-radius: 0;
  -moz-appearance: none;
  -webkit-appearance: none;
  appearance: none;
  background-color: var(--white);
}
.select-css::-ms-expand {
  display: none;
}
.select-css:hover {
  border-color: var(--dark-gray);
}
.select-css:focus {
  border-color: #aaa;
  box-shadow: 0 0 1px 3px rgba(59, 153, 252, 0.7);
  box-shadow: 0 0 0 3px -moz-mac-focusring;
  color: #222;
  outline: none;
}
.select-css option {
  font-weight: normal;
}

.iconwrapper {
  width: 100%;
}
.iconwrapper input {
  width: 80% !important;
}
.preview-icon {
  border: 1px solid #c9c9c9;
  margin-left: 10px;
  padding: 11px;
}

.save_cancel_buttons input {
  width: 49% !important;
}
.save_cancel_buttons input[type='submit'] {
  margin-right: 2%;
}

.tags-input-wrapper-default,
.tags-input-wrapper-default.active {
  padding: 0;
  background: none;
  border: none;
  border-radius: 0;
  border-color: #dbdbdb;
  box-shadow: none;
}

input[type='tag'] {
  display: none;
}
</style>
