import Vue from 'vue';
import mapWrapper, { MapWrapper } from '@/utils/mapWrapper';

declare module 'vue/types/vue' {
  interface Vue {
    $map: MapWrapper;
  }
}

Vue.prototype.$map = mapWrapper;
