import Vue from 'vue';
import { DbWrapper } from '@/utils/dbWrapper';
import { eventBus } from '@/eventBus';

declare module 'vue/types/vue' {
  interface Vue {
    $db: DbWrapper;
  }
}

Vue.prototype.$db = new DbWrapper();
Vue.prototype.$db.setLoginCallback(function() {
  eventBus.emit('showLoginScreen');
});
