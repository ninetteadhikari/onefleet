import Vue from 'vue';
import { Templates } from './constants/templates';
import { DbPosition, DbPositionsPerItem } from './types/db-position';
import { FilteredItemSection } from './types/item-filter';
import { PositionDocEnriched } from './types/map-item';
import { ReplayOptions } from './types/replay-options';

/**
 * The list of event bus events with the respective event arguments interface.
 */
interface EventBusEventsWithArguments {
  __test: {
    firstArg: number;
  };
  updateLastPositionTimestamp: {
    lastPositionTimestamp: string | null;
  };
  showLoginScreen: undefined;
  updateMainView: {
    mainView: string;
  };
  showSettings: undefined;
  createItem: {
    templateKey?: keyof typeof Templates.templates;
    positions?: PositionDocEnriched[];
  };
  showAir: undefined;
  showLog: undefined;
  closeModal: undefined;
  showItem: {
    itemId: string;
    positions?: DbPosition[] | PositionDocEnriched[];
  };
  updateExportItemId: {
    itemId: string | false;
  };
  updateLastChangeDate: {
    date: Date;
  };
  flyToPosition: {
    position: DbPosition;
  };
  startReplay: {
    replayData: ReplayOptions;
  };
  replayStarted: undefined;
  replayFinished: undefined;
  replayNextTicks: undefined;
  setItemLayers: {
    filteredItems: FilteredItemSection[];
    positionsPerItem: DbPositionsPerItem;
  };
}

/**
 * The event bus constructor.
 */
class EventBus extends Vue {
  /**
   * Trigger an event.
   * @param event - The event to trigger.
   * @param args - The event arguments.
   */
  public emit<
    T extends keyof EventBusEventsWithArguments,
    TArgs extends EventBusEventsWithArguments[T]
  >(event: T, ...args: TArgs extends undefined ? [] : [TArgs]): void {
    if (args.length > 0) {
      this.$emit(event, ...args);
    } else {
      this.$emit(event);
    }
  }

  /**
   * Add an event listener for the specified event.
   * @param event - The event to listen on.
   * @param callback - The callback to execute when the event is triggered.
   */
  public on<
    T extends keyof EventBusEventsWithArguments,
    TArgs extends EventBusEventsWithArguments[T]
  >(
    event: T,
    callback: (...args: TArgs extends undefined ? [] : [TArgs]) => void
  ): void {
    this.$on(event, callback);
  }
}

/**
 * @var The event bus instance.
 */
export const eventBus = new EventBus();
