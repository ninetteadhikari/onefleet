import { eventBus } from './eventBus';

describe('eventBus', () => {
  describe('.emit()/.on()', () => {
    it('triggers the callback passed to .on() on calling .emit() with the correct arguments', () => {
      const testValue = 123;
      let receivedValue;
      eventBus.on('__test', ({ firstArg }) => {
        receivedValue = firstArg;
      });
      eventBus.emit('__test', { firstArg: testValue });
      expect(receivedValue === testValue).toBe(true);
    });
  });
});
