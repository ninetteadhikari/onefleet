/**
 * Storage wrapper singleton class.
 */
class StorageWrapper {
  /**
   * Store an item in localStorage.
   * @param i - The storage item's key.
   * @param v - The value to store.
   */
  public set(i: string, v: string): void {
    return localStorage.setItem(i, v);
  }

  /**
   * Retrieve an item from localStorage.
   * @param i - The storage item's key.
   * @returns The storage item's value.
   */
  public get(i: string): string | null {
    return localStorage.getItem(i);
  }
}

export default new StorageWrapper();
