import * as L from 'leaflet';
import 'leaflet-draw';
import 'leaflet-mouse-position';
import { SARZones } from '../constants/sar-zones';
import storage from './storageWrapper';
import { SARZone } from '@/types/sar-zone';
import { MapItem } from '../types/map-item';
import { DbItem } from '@/types/db-item';
import { DbPosition, DbPositionsPerItem } from '@/types/db-position';
import { FilteredItemSection } from '@/types/item-filter';

/**
 * The mapWrapper is an abstraction layer from the underlying mapping backend. (Currently leaflet.js)
 * It also provides some convenience methods for recurring map-related tasks.
 */
export class MapWrapper {
  public map!: L.Map;
  public get_popup_contents_from_vue!: any;
  public loaded_items: {
    [index: string]: {
      marker: L.Marker<any>;
      line: L.Polyline<any, any>;
      lineCaptions: L.Marker<any>[];
      show_track?: boolean; // whether to show track on the map
      showing_on_layer?: string; // which map layer an item was added to
    };
  } = {};
  public sarZoneLayerGroup = new L.LayerGroup();
  public sarZoneIsVisible = true;
  public drawnAreasAndMarkers = new L.FeatureGroup();
  private overlayMaps: any; // LayersObject;
  private layers_control: L.Control.Layers | null = null;
  private classic_items_layer = new L.LayerGroup();

  /**
   * Initialises the map backend component.
   * @param mapId The id of the div element that this map will be placed in
   * @param popup_contents A callable that provides a div element for popups when requested.
   * @memberof mapWrapper
   */
  public init(mapId: string, popup_contents: any): void {
    this.get_popup_contents_from_vue = popup_contents;

    /** Map Zoom and Center */
    let mapcenter: [number, number];
    let mapzoom: number;
    try {
      const storageMapzoom = storage.get('mapzoom');
      const storageMapcenter = storage.get('mapcenter');
      if (storageMapzoom === null || storageMapcenter === null) {
        throw new Error(
          `mapzoom: ${storageMapzoom}, mapcenter: ${storageMapcenter}`
        );
      } else {
        mapzoom = parseInt(storageMapzoom, 10);
        mapcenter = JSON.parse(storageMapcenter);
      }
    } catch (err) {
      console.error('could not load mapcenter and zoom from localstorage', err);
      mapcenter = [38.57, 10.7];
      mapzoom = 5;
    }
    if (!mapzoom) mapzoom = 5;
    if (!mapcenter) mapcenter = [38.57, 10.7];

    /** prepare Tiles start */
    let onefleet_tiles = L.tileLayer('/MapTiles/{z}/{x}/{y}.png', {
      attribution:
        'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 10, // onefleet map tiles don't exist above 10
      minZoom: 3,
      noWrap: true,
      id: 'groundtile',
    });
    let openstreetmap_tiles = L.tileLayer(
      'http://{s}.tile.osm.org/{z}/{x}/{y}.png',
      {
        attribution:
          'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        minZoom: 3,
        noWrap: true,
        id: 'groundtile',
      }
    );
    let light_tiles = L.tileLayer(
      'https://s.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png',
      {
        attribution:
          'Map tiles by Carto, under CC BY 3.0. Data by <a href="https://www.openstreetmap.org/">OpenStreetMap</a>, under ODbL.',
        maxZoom: 18,
        minZoom: 3,
        noWrap: true,
        id: 'groundtile',
      }
    );

    let seamap_tiles = L.tileLayer(
      'http://tiles.openseamap.org/seamark/{z}/{x}/{y}.png',
      {
        attribution: '',
        maxZoom: 18,
        minZoom: 9, // seamap tiles don't exists below 9
        noWrap: true,
        id: 'openseamap',
        accessToken: '',
      }
    );
    /** prepare Tiles end */

    var baseMaps = {
      'Onefleet Map': onefleet_tiles,
      'OpenStreetMap ': openstreetmap_tiles,
      'mostly Greyscale': light_tiles,
    };
    this.overlayMaps = {
      'OpenSeamap ': seamap_tiles,
      // 'classic items layer': this.classic_items_layer,
      'SAR zones': this.sarZoneLayerGroup,
      'drawn Areas & Markers <hr>': this.drawnAreasAndMarkers,
    };
    var startingMaps = [
      localStorage.settings_maptiles == 'onefleet'
        ? onefleet_tiles
        : localStorage.settings_maptiles == 'greyscale'
        ? light_tiles
        : openstreetmap_tiles,
      this.drawnAreasAndMarkers, // always show this (empty) layer at startup time
      this.sarZoneLayerGroup, // activate here to show SAR zones at startup time
      // this.classic_items_layer, // the old way of how the items get on to the map
    ];
    if (localStorage.settings_openseamap == 'true')
      startingMaps.push(seamap_tiles);

    /** instantiate Map object start */
    this.map = L.map(mapId, {
      center: mapcenter,
      zoom: mapzoom,
      zoomSnap: 1.0,
      layers: startingMaps,
    });
    /** instantiate Map object end */

    /** Set up map control buttons */
    this.layers_control = L.control.layers(baseMaps, this.overlayMaps);
    this.layers_control.addTo(this.map);

    L.control.scale({ imperial: false }).addTo(this.map);

    /* Measurement Tools for measuring distances and angles */
    this._initMeasurementTool();

    //** SAR Zones setup (to be replaced with zones from database) */
    this._initSarZones(SARZones);
    // this.map.addControl(this._createSarZoneToggleControl());

    /* Toggle to create extra control to add a custom marker by coordinates */
    this.map.addControl(this.createCustomMarkerControl());

    /* Allows us to draw shapes on the map, including dropping markers and what to do when clicked. */
    this._initShapeDrawing();

    //** Add mouse coordinates */
    L.control
      .mousePosition({
        position: 'bottomright',
        emptyString: '',
        formatter: (lng: number, lat: number) => {
          return (
            this.asDMS({ lat: lat, lon: lng }) +
            ' | ' +
            this.asDD({ lat: lat, lon: lng })
          );
        },
      })
      .addTo(this.map);

    this.map.on('move', () => {
      storage.set('mapzoom', (this.map as any)._zoom);
      storage.set(
        'mapcenter',
        JSON.stringify([this.map.getCenter().lat, this.map.getCenter().lng])
      );
    });

    console.log('map initiated');
  }

  /* Measurement Tools for measuring distances and angles */
  private _initMeasurementTool() {
    (L.control as any)
      .polylineMeasure({
        // config follows https://developer.aliyun.com/mirror/npm/package/leaflet.polylinemeasure
        position: 'topleft',
        unit: 'nauticalmiles',
        measureControlTitleOn: 'Measure Distances and Angles', // Title for the control going to be switched on
        measureControlTitleOff: 'Clear Measurements', // Title for the control going to be switched off
        measureControlLabel: '&#8614;', // HTML to place inside the control. Default: &#8614;
        backgroundColor: '#8f8', // Background color for control when selected. Default: #8f8
        showBearings: true,
        // clearMeasurementsOnStop: false,
        // showClearControl: true,
        // showUnitControl: false,
      })
      .addTo(this.map);
  }

  /**
   * Creates a featureGroup with polygons of each given SAR Zone from the parameter.
   */
  private _initSarZones(sarZones: SARZone[]): void {
    // this.map.addLayer(this.sarZoneLayerGroup);

    for (let sarZoneObject of sarZones) {
      const coordinates = sarZoneObject.coordinates;
      const color = sarZoneObject.color;
      const tooltip = sarZoneObject.name;

      let sarZonePolygon = L.polygon(coordinates)
        //** fill the polygon with minimal transparency to force that the tooltip also opens, if user hovers over the whole SAR sone and not only over the colored bounding borders. */
        .setStyle({
          color: 'darkgrey',
          fill: true,
          fillColor: color,
          fillOpacity: 0.1,
          weight: 1,
          // dashArray: '4',
        })
        .bindTooltip(tooltip + ' SAR Zone');

      this.sarZoneLayerGroup.addLayer(sarZonePolygon);
    }
  }

  //* Add a toggle to create a new custom marker */
  private createCustomMarkerControl() {
    var toggleCustomMarkerControl = L.Control.extend({
      options: {
        position: 'topleft',
      },

      onAdd: () => {
        var container = L.DomUtil.create(
          'div',
          'leaflet-bar leaflet-control leaflet-control-custom'
        );

        let aTag = L.DomUtil.create(
          'a',
          'polyline-measure-unicode-icon',
          container
        );
        aTag.title = 'Place a marker at specific location';
        // Set icon
        L.DomUtil.addClass(aTag, 'el-icon-location-outline');

        // We must prevent the double click of the button, otherwise the map zoomes in if button is double clicked.
        container.ondblclick = e => e.stopImmediatePropagation();

        container.onclick = () =>
          this.get_popup_contents_from_vue('addmarker', {});

        return container;
      },
    });
    return new toggleCustomMarkerControl();
  }

  /** Method calls the flyTo method from leaflet.js
   * See also: https://leafletjs.com/reference-1.0.0.html
   * Sets the view of the map (geographical center and zoom) performing a smooth pan-zoom animation.
   * @param {[number, number]} positions The latitude/longitude coordinates.
   */
  public flyTo(positions: [number, number], target_layer_title?: string) {
    this.map.flyTo(positions);

    if (target_layer_title) {
      this.overlayMaps[target_layer_title]?.addTo(this.map);
    }
  }

  // Truncate value based on number of decimals
  private _round(num: number, len: number): number {
    return Math.round(num * Math.pow(10, len)) / Math.pow(10, len);
  }

  private _getDms(val: number, is_lat: boolean): string {
    let valDeg, valMin, valSec, hemi;

    if (is_lat) hemi = val >= 0 ? 'N' : 'S';
    else hemi = val >= 0 ? 'E' : 'W';

    val = Math.abs(val);
    valDeg = Math.floor(val);
    valMin = Math.floor((val - valDeg) * 60);
    valSec = Math.round((val - valDeg - valMin / 60) * 3600 * 10) / 10;
    return valDeg + 'º ' + valMin + "' " + valSec + '" ' + hemi;
  }

  // Public helper method to format UI position objects
  public asDMS(position: { lat: number; lon: number }): string {
    if (!position) return 'No Position';
    let latDms = this._getDms(position.lat, true);
    let lngDms = this._getDms(position.lon, false);
    return latDms + ', ' + lngDms;
  }

  // Public helper method to format UI position objects
  public asDD(position: { lat: number; lon: number }): string {
    if (!position) return 'No Position';
    let len = 5;
    return (
      '' +
      Math.round(position.lat * Math.pow(10, len)) / Math.pow(10, len) +
      'º, ' +
      Math.round(position.lon * Math.pow(10, len)) / Math.pow(10, len) +
      'º'
    );
  }

  /** Returns a textual representation of how much time has passed between two dates */
  public timeSince(
    date_past: string | number | Date,
    date_now: string | number | Date | null | undefined
  ) {
    //   date = new Date(date);
    let lastDate = new Date(date_past);
    let now = date_now ? new Date(date_now) : new Date();
    let seconds = Math.floor((now.getTime() - lastDate.getTime()) / 1000);

    let interval = Math.floor(seconds / 31536000);
    if (interval > 1) {
      return interval + ' years';
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
      return interval + ' months';
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
      return interval + ' days';
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
      return interval + ' hours';
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
      return interval + ' minutes';
    }
    return Math.floor(seconds) + ' seconds';
  }

  /** Returns a status light color that depends on how much time has passed */
  public colorSince(
    date_past?: string | number | Date | null | undefined,
    date_now?: string | number | Date | null | undefined
  ) {
    if (
      date_now &&
      Math.abs(new Date(date_now).getTime() - new Date().getTime()) > 60 * 1000 // more than a minute different from now
    )
      // we're looking into the past or future, so just show blue:
      return 'blue';
    else {
      if (date_past) {
        let now = date_now ? new Date(date_now) : new Date();
        let seconds = Math.floor(
          (now.getTime() - new Date(date_past).getTime()) / 1000
        );
        // less than 30 minutes:
        if (seconds > 0 && seconds <= 1800) return 'green';
        // less than 24 hours:
        else if (seconds <= 86400) return 'yellow';
        // more than 24 hours:
        else return 'red';
      } else {
        // no old date given / does not apply:
        return 'grey';
      }
    }
  }

  public formatTimestamp(
    the_datetime: Date | string | null | undefined,
    reference_datetime?: Date | string | null | undefined
  ) {
    if (!the_datetime) return null;
    the_datetime = new Date(the_datetime);
    let time_string = the_datetime.toLocaleTimeString('en-GB', {
      hour: 'numeric',
      minute: 'numeric',
      timeZoneName: 'short',
    });
    let date_string = the_datetime.toLocaleDateString('en-GB', {
      weekday: 'short',
      day: '2-digit',
      month: 'short',
      year: 'numeric',
    });
    if (
      reference_datetime &&
      the_datetime.toDateString() == new Date(reference_datetime).toDateString()
    )
      return time_string;
    else return time_string + ` (${date_string})`;
  }

  // Generate popup content based on layer type
  // - Returns HTML string, or null if unknown object
  private _getDrawnShapePopupContent(layer): string | null | any {
    let latlngs, distance: number;
    if (
      layer instanceof L.Marker ||
      layer instanceof L.CircleMarker ||
      layer instanceof L.Circle
    )
      // Marker - add lat/long
      return map_object =>
        this.get_popup_contents_from_vue('drawn_marker_popup', {
          position: {
            lat: map_object.getLatLng().lat,
            lon: map_object.getLatLng().lng,
          },
          // radius: map_object.getRadius ? map_object.getRadius() : 0,
          item_id: '',
          item_title: 'Unknown Item',
        });
    else if (layer instanceof L.Polygon)
      // Rectangle/Polygon - area
      return map_object => {
        let area_positions = map_object._defaultShape
          ? map_object._defaultShape()
          : map_object.getLatLngs();
        let area_geodesic = L.GeometryUtil.geodesicArea(area_positions);
        let popup_data: {
          area_readable: String;
          positions: { lat: number; lon: number }[];
        } = {
          area_readable: L.GeometryUtil.readableArea(area_geodesic, true),
          positions: this._convertPositionTypes(area_positions),
        };
        return this.get_popup_contents_from_vue('drawn_area_popup', popup_data);
      };
    else if (layer instanceof L.Polyline)
      // Polyline - distance
      return () => {
        let popup_data: {
          distance_in_meters: number;
          positions: { lat: number; lon: number }[];
        } = {
          distance_in_meters: -1,
          positions: [],
        };
        layer = layer as any;
        latlngs = layer._defaultShape
          ? layer._defaultShape()
          : layer.getLatLngs();
        distance = 0;
        if (latlngs.length > 1) {
          for (var i = 0; i < latlngs.length - 1; i++) {
            distance += latlngs[i].distanceTo(latlngs[i + 1]);
          }
          popup_data.distance_in_meters = distance;
          popup_data.positions = this._convertPositionTypes(latlngs);
        }
        return this.get_popup_contents_from_vue(
          'drawn_track_popup',
          popup_data
        );
      };
    return null;
  }

  /**
   * Converts leaflet LatLng objects to general objects.
   * Note: `lng` gets changed to `lon` because the consuming code expects that.
   */
  private _convertPositionTypes(
    latlngs: L.LatLng[]
  ): { lat: number; lon: number }[] {
    return latlngs.map(pos => ({ lat: pos.lat, lon: pos.lng }));
  }

  /**
   * Sets up the ability to draw shapes on the map, including dropping markers and what to do when clicked.
   */
  private _initShapeDrawing(): void {
    /* Add a layer for drawing shapes in. These can then be added to items as positions, areas, etc */

    //** Add the standard Map actions. */
    let drawing_toolbar = new L.Control.Draw({
      edit: {
        featureGroup: this.drawnAreasAndMarkers,
        poly: {
          allowIntersection: true,
        },
      } as any,
      draw: {
        polyline: false,
        // polyline: {
        //   metric: false,
        //   feet: false,
        //   nautic: true,
        // },
        polygon: {
          allowIntersection: true,
          showArea: true,
        },
        circle: false,
        circlemarker: false,
      },
    });
    this.map.addControl(drawing_toolbar);

    // Object created - bind popup to layer, add to feature group
    this.map.on(L.Draw.Event.CREATED, (event: L.LayerEvent) => {
      var drawnMarkerAreaOrTrack = event.layer;
      var content = this._getDrawnShapePopupContent(drawnMarkerAreaOrTrack);
      if (content !== null) {
        drawnMarkerAreaOrTrack.bindPopup(content);
      }
      this.drawnAreasAndMarkers.addLayer(drawnMarkerAreaOrTrack);
      this.drawnAreasAndMarkers.addTo(this.map); // ensure the draw-layer is always shown whenever something is drawn
    });
  }

  /**
   * Prepares the given item for UI by converting templates to known base
   * templates, defining icons, and cropping the loaded number of positions
   * to an amount that can be handled by interactive display.
   */
  public prepareTemplatedItem(item: MapItem): MapItem {
    let max_positions = localStorage.settings_map_track_length || 100;
    let max_track_type =
      localStorage.settings_max_track_type || 'number_of_positions';

    if (typeof max_positions == 'string')
      max_positions = parseInt(max_positions);

    if (max_track_type == 'number_of_positions') {
      // filter out positions by number of positions (default: 100 positions)
      if (item.positions.length > max_positions) {
        // crop number of positions to max_positions
        item.positions.splice(0, item.positions.length - max_positions);
      }
    } else if (max_track_type == 'number_of_days') {
      // filter out positions by number of days in the past (defaut: 100 days)
      let min_date = new Date();
      min_date.setDate(min_date.getDate() - max_positions);
      item.positions = item.positions.filter(position => {
        return min_date < new Date(position.doc.timestamp);
      });
    } else if (max_track_type == 'date_range') {
      // filter out positions by date range from local storage
      let min_date = new Date(localStorage.settings_track_startdate);
      let max_date = new Date(localStorage.settings_track_enddate);
      item.positions = item.positions.filter(position => {
        let date = new Date(position.doc.timestamp);
        return max_date > date && date > min_date;
      });
    }

    return item;
  }

  /**
   * Removes all layers from the map, thereby clearing it.
   * @deprecated this should no longer be needed
   */
  public clearMap(): void {
    let i = 0;
    this.map.eachLayer(layer => {
      if (i > 0) this.map.removeLayer(layer);
      i++;
    });
  }

  public generateLineCaptions(item: MapItem): L.Marker[] {
    /*let max_length = 5;
    item.positions.slice(-1 * max_length);*/
    var markers: any = [];
    if (item.positions.length > 0) {
      for (var i in item.positions) {
        var v = item.positions[i];
        if (v.doc.lat && v.doc.lon) {
          let date = new Date(v.doc.timestamp);
          let caption =
            date.toISOString().slice(0, 10) +
            ' - ' +
            String(date.getHours()).padStart(2, '0') +
            ':' +
            String(date.getMinutes()).padStart(2, '0');
          let icon = L.divIcon({
            className: 'lineCaption',
            html: '<div>' + caption + '</div>',
          });
          //let marker = new L.marker([v.doc.lat, v.doc.lon], { opacity: 0.5 }); //opacity may be set to zero

          let marker = L.marker([v.doc.lat, v.doc.lon], { icon: icon });
          markers.push(marker);
        } else {
          throw new Error(
            'An array of item lineCaptions needs at least one defined position.'
          );
        }
      }

      return markers;
    } else {
      throw new Error(
        'An array of item lineCaptions needs at least one position.'
      );
    }
  }

  /**
   * Return a new leaflet.Polyline consisting of the positions of the item.
   */
  public generateLine(item: MapItem): L.Polyline {
    // use only those positions which actually have the fields "lat" and "lon" defined:
    var pointList: L.LatLngExpression[] = item.positions
      // .filter(v => v.doc.lat && v.doc.lon)
      .map(v => [v.doc.lat, v.doc.lon]);

    if (pointList.length < 1) {
      throw new Error('An item track needs at least one defined position.');
    } else {
      let lastKnownPosition = item.positions[item.positions.length - 1];

      // color should always be defined: see this._buildMapItemFromDbItem()
      let color: string;
      if (typeof item.doc.properties.color != 'undefined')
        color = item.doc.properties.color;
      else
        color =
          '#' + ((Math.random() * 0xffffff) << 0).toString(16).padStart(6, '0');

      let track = new L.Polyline(pointList, {
        color: color,
        weight: 3,
        opacity: 1,
        smoothFactor: 1,
      });

      // Use the item's identifier (and its name if applicable) as Pop-up
      let popuptitle = (item.doc.identifier || '').toString();
      if (item.doc.properties.name) {
        popuptitle = item.doc.properties.name;
      } else {
        popuptitle = item.id;
      }
      // bind the popup content to the marker. Used by openPopup() below
      track.bindPopup(() =>
        this.get_popup_contents_from_vue('item_track_popup', {
          item_id: item.id,
          item_title: popuptitle,
          latest_position: lastKnownPosition.doc,
        })
      );

      return track;
    }
  }

  /**
   * Generate marker icon HTML content string, using a stored PNG image.
   * @param rotation
   * @param size The size of the icon
   * @param caption The caption to display near the maker. May be an empty string.
   * @returns A HTML String that can be given to a leaflet divIcon constructor.
   */
  private _createImageMarkerHTML(
    rotation: number,
    size: number,
    caption: string
  ): string {
    var css_style = {
      transform: 'rotate(' + rotation + 'deg)',
      width: size + 'px',
      height: size + 'px',
      'margin-left': size / -2 + 'px',
      'margin-top': size / -2 + 'px',
    };
    let iconimg = document.createElement('img');
    iconimg.src = '/gfx/icons/cursor.png';
    for (var k in css_style) {
      iconimg.style.setProperty(k, css_style[k]);
    }
    let iconhtml = document.createElement('div');
    iconhtml.appendChild(iconimg);
    iconhtml.append(caption);
    return iconhtml.outerHTML;
  }

  /**
   * Generate marker icon HTML content string, using any unicode character.
   * @param character The unicode character to use as icon. Some rotation and scaling may be necessary.
   * @param rotation The rotation of the icon. May consist of item's heading plus some constant rotation.
   * @param size The size of the icon. Currently in unit 'pt', but may change.
   * @param color The color for the icon. By using unicode characters, we can easily reflect boat color by icon color.
   * @param caption The caption to display near the maker. May be an empty string.
   * @returns A HTML String that can be given to a leaflet divIcon constructor.
   */
  private _createCharacterMarkerHTML(
    character: string,
    rotation: number,
    size: number,
    color: string,
    caption: string
  ): string {
    let iconhtml = document.createElement('div');

    let iconimg = document.createElement('div');
    iconimg.textContent = character;
    iconimg.style.fontSize = size + 'pt';
    iconimg.style.transform = 'rotate(' + rotation + 'deg)';
    iconimg.style.color = color;
    iconimg.style.width = size + 'pt';
    iconimg.style.textAlign = 'center';
    // iconimg.style.webkitTextStrokeColor = 'black';
    // iconimg.style.webkitTextStrokeWidth = '0.3px';
    iconhtml.appendChild(iconimg);

    if (caption) {
      let span = document.createElement('span');
      span.className = 'itemCaption';
      span.append(caption);
      iconhtml.appendChild(span);
    }

    iconhtml.style.marginLeft = '-15px';
    iconhtml.style.marginTop = '-22px';
    return iconhtml.outerHTML;
  }

  /**
   * Generate marker icon HTML content string, using any unicode character.
   * @param character The unicode character to use as icon. Some rotation and scaling may be necessary.
   * @param rotation The rotation of the icon. May consist of item's heading plus some constant rotation.
   * @param size The size of the icon. Currently in unit 'pt', but may change.
   * @param color The color for the icon. By using unicode characters, we can easily reflect boat color by icon color.
   * @param caption The caption to display near the maker. May be an empty string.
   * @returns A HTML String that can be given to a leaflet divIcon constructor.
   */
  private _createSVGMarkerHTML(
    icon_type: string,
    rotation: number,
    size: number,
    color: string,
    caption: string
  ): string {
    let iconhtml = document.createElement('div');

    let iconimg = document.createElement('div');
    iconimg.className = 'fas fa-' + icon_type;
    // iconimg.style.fontSize = size + 'pt';
    iconimg.style.transform = 'rotate(' + rotation + 'deg)';
    iconimg.style.color = color;
    // iconimg.style.width = size + 'pt';
    // iconimg.style.textAlign = 'center';
    iconimg.style.strokeWidth = '1px';
    iconimg.style.webkitTextStrokeColor = 'black';
    iconimg.style.webkitTextStrokeWidth = '1px';
    iconhtml.appendChild(iconimg);

    if (caption) {
      let span = document.createElement('span');
      span.className = 'itemCaption';
      span.append(caption);
      iconhtml.appendChild(span);
    }

    iconhtml.style.marginLeft = '-7px';
    iconhtml.style.marginTop = '-10px';
    return iconhtml.outerHTML;
  }

  /**
   * Return a new leaflet.Marker at the last position of the item.
   * The marker uses a "cursor" icon and is rotated to reflect the heading
   * of the item at its last known position.
   *
   * This function also configures the following event handlers:
   * @see L.Marker.openPopup() The function to call on mouseover.
   * @see L.Marker.closePopup() The function to call on mouseout.
   */
  public generateMarker(item: MapItem): L.Marker {
    if (!item.positions || item.positions.length < 1) {
      throw new Error('An item marker needs at least one position.');
    }
    // use the last known position for the marker:
    var lastKnownPosition = item.positions[item.positions.length - 1];
    // if (!lastKnownPosition.doc.lat || !lastKnownPosition.doc.lon) {
    //   throw new Error('An item marker needs at least one defined position.');
    // }

    let caption = '';
    if (localStorage.settings_showcaptions === 'true') {
      if (item.doc.properties.name) {
        caption = item.doc.properties.name;
      } else {
        caption = item.doc.template + ' ' + item.doc.identifier;
      }
    }

    let icon;
    if (item.doc.properties.icon) {
      // use item-specific icon if given
      // icon = L.divIcon({
      //   className: 'vehicle-marker',
      //   html:
      //     '<div><span class="fas fa-' +
      //     item.doc.properties.icon +
      //     '"></span>' +
      //     caption +
      //     '</div>',
      // });
      icon = L.divIcon({
        className: 'vehicle-marker',
        html: this._createSVGMarkerHTML(
          item.doc.properties.icon,
          (lastKnownPosition.doc.heading || 0) - 90,
          20,
          item.doc.properties.color || 'black',
          caption
        ),
      });
    } else if (item.doc.template == 'case') {
      // set default case icon
      icon = L.divIcon({
        className: 'case-marker',
        html: this._createCharacterMarkerHTML(
          '⊳', // unicode 0x22B3 / html &#8883;
          (lastKnownPosition.doc.heading || 0) - 90,
          20,
          item.doc.properties.color || 'black',
          caption
        ),
      });
    } else if (item.doc.template == 'landmark') {
      // set default landmark icon
      icon = L.divIcon({
        className: 'landmark-marker',
        html: this._createCharacterMarkerHTML(
          '⟟', // unicode 0x27DF / html &#10207;
          lastKnownPosition.doc.heading || 0,
          20,
          item.doc.properties.color || 'black',
          caption
        ),
      });
    } else if (item.doc.template == 'aircraft') {
      // set default airplane icon
      icon = L.divIcon({
        className: 'vehicle-marker',
        html: this._createCharacterMarkerHTML(
          '✈︎', // unicode 0x2708 / html &#9992;
          lastKnownPosition.doc.heading || 0,
          20,
          item.doc.properties.color || 'black',
          caption
        ),
      });
    } else if (
      item.doc.template == 'vehicle' ||
      typeof item.doc.template === 'undefined' // TODO fix (vehicle) items by using classes
    ) {
      // set default vehicle icon
      icon = L.divIcon({
        className: 'vehicle-marker',
        html: this._createCharacterMarkerHTML(
          '⩥', // unicode 0x2A65 / html &#10853;
          (lastKnownPosition.doc.heading || 0) - 90,
          20,
          item.doc.properties.color || 'black',
          caption
        ),
      });
    } else {
      // lazy fail: Do we want to raise an error here instead?
      console.error(
        'Unknown item template while generating marker icon. Using "*" for now. Please fix issue #131 soon! '
      );
      icon = L.divIcon({
        className: 'vehicle-marker',
        html: this._createCharacterMarkerHTML(
          '*',
          (lastKnownPosition.doc.heading || 0) - 90,
          40,
          item.doc.properties.color || 'black',
          caption
        ),
      });
    }

    // create a new marker with the given lat/lon position and icon
    let marker = L.marker(
      [lastKnownPosition.doc.lat, lastKnownPosition.doc.lon],
      { icon: icon }
    );
    // Use the item's identifier (and its name if applicable) as Pop-up
    let popuptitle = (item.doc.identifier || '').toString();
    if (item.doc.properties.name) {
      popuptitle = item.doc.properties.name;
    } else {
      popuptitle = item.id;
    }
    // bind the popup content to the marker. Used by openPopup() below
    marker.bindPopup(() =>
      this.get_popup_contents_from_vue('item_marker_popup', {
        item_id: item.id,
        item_title: popuptitle,
        latest_position: lastKnownPosition.doc,
      })
    );
    return marker;
  }

  /**
   * Adds the given item onto the map.
   * The given item will be prepared and replaces any previous
   * instances of this item on the map.
   *
   * @see this.prepareTemplatedItem() Used to prepare item for display on map.
   * @see this.generateLine() Used to generate a polyline for the item.
   * @see this.generateMarker() Used to generate a marker for the item
   */
  private _createItemMarkersTracksAreas(
    item: MapItem,
    target_layer_title?: string
  ): void {
    item = this.prepareTemplatedItem(item);

    if (item.positions.length > 0) {
      let loaded_item_handles = {
        marker: this.generateMarker(item),
        line: this.generateLine(item),
        lineCaptions:
          localStorage.settings_positiontimestamps == 'true'
            ? this.generateLineCaptions(item)
            : [],
        show_track: false, // see this._initialTrackVisibility()
        showing_on_layer: target_layer_title,
      };
      this.loaded_items[item.id] = loaded_item_handles;
    }
  }

  /**
   * Updates a loaded item's position on the map and ensures it is visible.
   *
   * @see this.addItemToMap() Used to add an item to the map if not present.
   * @returns {boolean} False if the item has no positions; undefined otherwise.
   */
  public updateItemPosition(item: MapItem, target_layer_title?: string): void {
    if (item.positions.length < 1) return;

    if (!this.loaded_items[item.id]) {
      this._createItemMarkersTracksAreas(item, target_layer_title);
    }

    let latestPos = item.positions[item.positions.length - 1].doc;

    if (this.loaded_items[item.id].marker) {
      this.loaded_items[item.id].marker.setLatLng([
        latestPos.lat,
        latestPos.lon,
      ]);
    }

    if (this.loaded_items[item.id].line) {
      if (latestPos)
        this.loaded_items[item.id].line.addLatLng([
          latestPos.lat,
          latestPos.lon,
        ]);
    }

    this._showItemMarkerOnMap(item.id, target_layer_title);
    this._showItemTrackOnMap(item.id, target_layer_title);
  }

  private _buildMapItemFromDbItem(
    base_item: DbItem,
    item_positions: Array<DbPosition>
  ): MapItem {
    return {
      id: base_item._id,
      doc: {
        template: base_item.template,
        identifier: base_item.identifier,
        properties: {
          name:
            base_item.properties.name !== undefined
              ? base_item.properties.name
              : base_item.template + ' ' + base_item.identifier,
          color:
            base_item.properties.color !== undefined
              ? base_item.properties.color
              : base_item.properties.boat_color !== undefined
              ? base_item.properties.boat_color
              : 'Black',
          icon:
            base_item.properties.air === 'true'
              ? 'plane'
              : base_item.properties.icon ?? '',
        },
      },
      positions:
        item_positions != undefined
          ? item_positions.map(item => ({ doc: item })).reverse() // the old implementation assumes that the newest position is at the back of the array
          : [],
    };
  }

  /**
   * Sets a map item's visibility and adds any new positions it might have gained
   * If the item is not on the map yet, add it to the map.
   */
  public updateItemOnMap(
    base_item: DbItem,
    item_positions: Array<DbPosition>,
    show_item: boolean,
    target_layer_title?: string
  ): void {
    if (show_item) {
      // todo: only build this when really necessary
      let map_item = this._buildMapItemFromDbItem(base_item, item_positions);
      this.updateItemPosition(map_item, target_layer_title);
    } else this.hideItem(base_item._id, target_layer_title);
  }

  /**
   * Creates a marker at specific coordinates.
   */
  public addMarkerByCoordinates(lat: number, lon: number): void {
    this.flyTo([lat, lon]);
    let coords = new L.LatLng(lat, lon);
    var marker = new L.Marker(coords);
    var layer = marker;
    var content = this._getDrawnShapePopupContent(layer);
    if (content !== null) {
      layer.bindPopup(content);
    }
    this.drawnAreasAndMarkers.addLayer(layer);
    this.drawnAreasAndMarkers.addTo(this.map); // ensure the draw-layer is always shown whenever something is drawn
  }

  /**
   * Hides the given item's marker from the map.
   */
  public hideItem(item_id: string, target_layer_title?: string): void {
    this.hideItemMarker(item_id, target_layer_title);
    this.hideItemTrack(item_id, target_layer_title);
  }

  /**
   * Hides the given item's marker from the map.
   */
  public hideItemMarker(item_id: string, target_layer_title?: string): void {
    let targetLayerGroup: L.LayerGroup = target_layer_title
      ? this.overlayMaps[target_layer_title]
      : this.classic_items_layer;

    let item = this.loaded_items[item_id];
    if (item) {
      if (item.marker) {
        targetLayerGroup.removeLayer(item.marker);
      }
    }
  }

  /**
   * Hides the given item's track from the map.
   */
  public hideItemTrack(item_id: string, target_layer_title?: string): void {
    let targetLayerGroup: L.LayerGroup = target_layer_title
      ? this.overlayMaps[target_layer_title]
      : this.classic_items_layer;

    let item = this.loaded_items[item_id];
    if (item) {
      if (item.line) {
        targetLayerGroup.removeLayer(item.line);
      }
      for (let i in item.lineCaptions) {
        targetLayerGroup.removeLayer(item.lineCaptions[i]);
      }
    }
  }

  private _showItemMarkerOnMap(item_id: string, target_layer_title?: string) {
    let targetLayerGroup = target_layer_title
      ? this.overlayMaps[target_layer_title]
      : this.classic_items_layer;

    if (this.loaded_items[item_id].marker) {
      this.loaded_items[item_id].marker.addTo(targetLayerGroup);
    }
  }

  private _showItemTrackOnMap(item_id: string, target_layer_title?: string) {
    let targetLayerGroup = target_layer_title
      ? this.overlayMaps[target_layer_title]
      : this.classic_items_layer;

    if (this.loaded_items[item_id]?.show_track) {
      if (this.loaded_items[item_id].line) {
        this.loaded_items[item_id].line.addTo(targetLayerGroup);
      }

      if (this.loaded_items[item_id].lineCaptions) {
        for (let i in this.loaded_items[item_id].lineCaptions) {
          this.loaded_items[item_id].lineCaptions[i].addTo(targetLayerGroup);
        }
      }
    }
  }

  /**
   * Calculates the geographical distance between two points on the map, in meters.
   * @returns {number} The distance in meters (CI unit)
   */
  public getDistance(
    point1: { lat: number; lon: number },
    point2: { lat: number; lon: number }
  ): number {
    let latlng1 = L.latLng(point1.lat, point1.lon);
    let latlng2 = L.latLng(point2.lat, point2.lon);
    return latlng1.distanceTo(latlng2);
  }

  /**
   * Tell the map to hide or show the track for a given item.
   * @param item_id
   * @param visibility_value
   */
  public setItemTrackShowing(item_id: string, visibility_value: boolean) {
    if (this.loaded_items[item_id]) {
      this.loaded_items[item_id].show_track = visibility_value;
      if (visibility_value) {
        this._showItemTrackOnMap(
          item_id,
          this.loaded_items[item_id].showing_on_layer
        );
      } else {
        this.hideItemTrack(
          item_id,
          this.loaded_items[item_id].showing_on_layer
        );
      }
    }
  }

  /**
   * Ask the map if a given ite's track is currently being displayed.
   * @param item_id
   */
  public isItemTrackShowing(item_id: string): boolean {
    return this.loaded_items[item_id]?.show_track || false;
  }

  /**
   * Ask the map which layer a given item is being shown on.
   * This is useful for item popups to that users can be told which filter
   * they need to deactivate if they want to hide a given item.
   * @param item_id The id of the item in question.
   */
  public getShowingOnLayer(item_id: string): string | undefined {
    return this.loaded_items[item_id]?.showing_on_layer;
  }

  /**
   * This creates a map layer per FilteredItemSection, and fills the new layer with
   * markers and polylines each base_items. For each base_item, the positions_per_item
   * are used for placing the markers on the ap and for drawing any required tracks.
   * @param filtered_base_items An array of FilteredItemSections where each section
   *   contains the array of base_items (i.e. items without positions as in the database)
   *   that fit the current filter sections for that section.
   * @param positions_per_item A mapping from item ID to an array of database positions.
   */
  public setItemSections(
    filtered_base_items: Array<FilteredItemSection>,
    positions_per_item: DbPositionsPerItem
  ): void {
    for (
      let section_id = 0;
      section_id < filtered_base_items.length;
      section_id++
    ) {
      const filter_section: FilteredItemSection =
        filtered_base_items[section_id];
      // add item section as a new layer if it does not exist yet:
      if (!Object.keys(this.overlayMaps).includes(filter_section.title)) {
        let section_group = new L.LayerGroup();
        this.overlayMaps[filter_section.title] = section_group;
        if (filter_section.filter_group_settings?.selectable_on_map)
          this.layers_control?.addOverlay(section_group, filter_section.title);
        if (filter_section.filter_group_settings?.initially_selected_on_map)
          section_group.addTo(this.map);
      }
      // update items within this layer / section of filtered items;
      if (filter_section.filter_group_settings?.selectable_on_map) {
        let section_group: L.LayerGroup = this.overlayMaps[
          filter_section.title
        ];
        section_group.clearLayers();
        for (let i = 0; i < filter_section.base_items.length; i++) {
          const base_item = filter_section.base_items[i];
          const item_positions = positions_per_item[base_item.identifier];
          const first_appearance_on_map = !this.loaded_items[base_item._id];
          this.updateItemOnMap(
            base_item,
            item_positions,
            true,
            filter_section.title
          );
          if (first_appearance_on_map) {
            this.setItemTrackShowing(
              base_item._id,
              this._initialTrackVisibility(base_item)
            );
          }
        }
      }
    }
  }

  /**
   * Heuristic to decide which item tracks should initially be visible on the map.
   * This may be overridden by user preferences in the future.
   * @param base_item The item to look into for deciding initial track visibility.
   */
  private _initialTrackVisibility(base_item: DbItem): boolean {
    if (base_item.properties['status'])
      return base_item.properties['status'] != 'closed';
    else return false;
    // else return (
    //     base_item.properties['category'] == 'civilfleet' &&
    //     base_item.properties['active'] == 'true'
    //   );
  }
}

export default new MapWrapper();
